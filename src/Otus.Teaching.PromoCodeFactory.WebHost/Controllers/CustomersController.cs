﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Specifications;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<PromoCode> _promoCodeRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomersController(IRepository<Customer> customerRepository,
            IRepository<PromoCode> promoCodeRepository, IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _promoCodeRepository = promoCodeRepository;
            _preferenceRepository = preferenceRepository;
        }
        /// <summary>
        /// Получить данные всех клиентов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<CustomerShortResponse>>> GetCustomersAsync()
        {
            var customers = await _customerRepository.GetAllAsync();

            var customerShortResponseList = customers
                .Select(x => new CustomerShortResponse(x))
                .ToList();

            return Ok(customerShortResponseList);
        }
        
        /// <summary>
        /// Получить данные клиента по id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}", Name = "GetCustomerByIdAsync")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customerSpec = new CustomerWithAllNavigationPropertiesSpecification(id);
            var customer = await _customerRepository.GetBySpecAsync(customerSpec);

            if (customer == null)
            {
                return NotFound("Клиент не найден");
            }

            return Ok(new CustomerResponse(customer));
        }
        
        /// <summary>
        /// Добавить нового клиента
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<CustomerResponse>> CreateCustomerAsync(
            [FromBody] CreateOrEditCustomerRequest request)
        {
            if (request == null)
            {
                return BadRequest("Customer object is null");
            }

            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid model object");
            }

            Customer customer;
            
            if (request.PreferenceIds != null && request.PreferenceIds.Any())
            {
                customer = request.ToCustomer(await _preferenceRepository.GetByIdAsync(request.PreferenceIds));
            }
            else
            {
                customer = request.ToCustomer();
            }

            await _customerRepository.AddAsync(customer);
            
            var addedCustomerSpec = new CustomerWithAllNavigationPropertiesSpecification(customer.Id);
            var addedCustomer = await _customerRepository.GetBySpecAsync(addedCustomerSpec);

            return CreatedAtRoute("GetCustomerByIdAsync",
                new {id = customer.Id}, new CustomerResponse(addedCustomer));
        }
        
        /// <summary>
        /// Заменить контент клиента по Id
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id:guid}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, [FromBody] CreateOrEditCustomerRequest request)
        {
            if (request == null)
            {
                return BadRequest("Customer object is null");
            }

            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid model object");
            }

            var customerSpec = new CustomerWithCustomerPreferencesSpecification(id);
            var customer = await _customerRepository.GetBySpecAsync(customerSpec);

            if (customer == null)
            {
                return NotFound("Клиент не найден");
            }

            Customer modifiedCustomer;
            
            if (request.PreferenceIds != null && request.PreferenceIds.Any())
            {
                modifiedCustomer = request.ToCustomer(
                    await _preferenceRepository.GetByIdAsync(request.PreferenceIds), customer);
            }
            else
            {
                modifiedCustomer = request.ToCustomer(inputCustomer: customer);
            }
            
            await _customerRepository.ReplaceAsync(modifiedCustomer);

            return NoContent();
        }

        /// <summary>
        /// Удалить клиента
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            var customerSpec = new CustomerWithCustomerPreferencesAndPromoCodesSpecification(id);
            var customer = await _customerRepository.GetBySpecAsync(customerSpec);

            if (customer == null)
            {
                return NotFound("Клиент не найден");
            }

            await _customerRepository.RemoveAsync(customer);

            return NoContent();
        }
    }
}
