﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Specifications;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromoCodesController
        : ControllerBase
    {
        private readonly IRepository<PromoCode> _promoCodesRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IRepository<CustomerPreference> _customerPreferenceRepository;

        public PromoCodesController(IRepository<PromoCode> promoCodesRepository,
            IRepository<Preference> preferenceRepository,
            IRepository<CustomerPreference> customerPreferenceRepository)
        {
            _promoCodesRepository = promoCodesRepository;
            _preferenceRepository = preferenceRepository;
            _customerPreferenceRepository = customerPreferenceRepository;
        }
        
        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromoCodesAsync()
        {
            var promoCodes = await _promoCodesRepository.GetAllAsync();

            var response = promoCodes
                .Select(pc => new PromoCodeShortResponse(pc))
                .ToList();

            return Ok(response);
        }
        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GivePromoCodesToCustomersWithPreferenceAsync(
            [FromBody] GivePromoCodeRequest request)
        {
            if (request == null)
            {
                return BadRequest($"{nameof(GivePromoCodeRequest)} object is null");
            }

            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid model object");
            }

            var preferenceSpecParams = new PreferenceSpecParams()
            {
                ExactNameMatch = request.Preference
            };

            var preferenceSpec = new PreferenceWithFilterSpecification(preferenceSpecParams);
            var preference = await _preferenceRepository.GetBySpecAsync(preferenceSpec);

            if (preference == null)
            {
                return NotFound("Указанное предпочтение клиента не найдено");
            }

            var customerPreferenceSpecParams = new CustomerPreferenceSpecParams()
            {
                PreferenceId =  preference.Id
            };

            var customerPreferenceSpec = new CustomerPreferenceWithFilterSpecification(customerPreferenceSpecParams);
            var customerPreferences = (await _customerPreferenceRepository
                .GetAllBySpecAsync(customerPreferenceSpec)).ToList();

            if (!customerPreferences.Any())
            {
                return NotFound("Не найдено клиентов с указанным предпочтением");
            }

            var promoCodes = customerPreferences
                .Select(cp => request.ToPromoCode(
                    preferenceId: cp.PreferenceId, customerId: cp.CustomerId))
                .ToList();
            
            await _promoCodesRepository.AddRangeAsync(promoCodes);
            
            var promoCodesResponse = (await _promoCodesRepository.GetByIdAsync(
                promoCodes.Select(x => x.Id)));

            var response = promoCodesResponse
                .Select(pc => new PromoCodeShortResponse(pc))
                .ToList();
            
            return Ok(response);
        }
    }
}
