﻿using System;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class PreferenceResponse
    {
        public Guid Id { get; set; }
        public string Name { get; set; }

        public PreferenceResponse(Preference preference)
        {
            Id = preference.Id;
            Name = preference.Name;
        }
    }
}
