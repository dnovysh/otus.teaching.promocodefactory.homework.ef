﻿using System;
using System.ComponentModel.DataAnnotations;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class GivePromoCodeRequest
    {
        [StringLength(100, ErrorMessage = "Длина строки должна быть не более 100 символов")]
        public string ServiceInfo { get; set; }

        [StringLength(100, ErrorMessage = "Длина строки должна быть не более 100 символов")]
        public string PartnerName { get; set; }

        [Required (ErrorMessage = "Не указан промокод")]
        [StringLength(20, ErrorMessage = "Длина строки должна быть не более 20 символов")]
        public string PromoCode { get; set; }
        
        [Required (ErrorMessage = "Не указано предпочтение клиента")]
        [StringLength(50, ErrorMessage = "Длина строки должна быть не более 50 символов")]
        public string Preference { get; set; }

        public PromoCode ToPromoCode(Guid preferenceId, Guid customerId)
        {
            return new PromoCode(
                id: Guid.NewGuid(), 
                code: PromoCode,
                serviceInfo: ServiceInfo,
                beginDate: DateTimeOffset.Now,
                endDate: DateTimeOffset.Now.AddMonths(1),
                partnerName: PartnerName,
                preferenceId: preferenceId,
                customerId: customerId 
            );
        }
    }
}
