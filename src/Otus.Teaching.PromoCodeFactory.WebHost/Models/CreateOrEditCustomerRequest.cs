﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class CreateOrEditCustomerRequest
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public List<Guid> PreferenceIds { get; set; }

        public Customer ToCustomer(IEnumerable<Preference> preferences = null, Customer inputCustomer = null)
        {
            var preferenceList = preferences?.ToList();
            
            var customer = inputCustomer ?? new Customer()
            {
                Id = Guid.NewGuid()
            };
            
            customer.FirstName = FirstName;
            customer.LastName = LastName;
            customer.Email = Email;
            
            if (preferenceList == null || !preferenceList.Any())
            {
                customer.CustomerPreferences?.Clear();
                return customer;
            }

            if (customer.CustomerPreferences == null)
            {
                customer.CustomerPreferences = new List<CustomerPreference>();
            }

            if (!customer.CustomerPreferences.Any())
            {
                preferenceList.ForEach(p =>
                {
                    customer.CustomerPreferences.Add(new CustomerPreference()
                    {
                        Id = Guid.NewGuid(),
                        CustomerId = customer.Id,
                        PreferenceId = p.Id
                    });
                });
            }
            
            var itemsToRemove = customer.CustomerPreferences
                .Where(cp => preferenceList.All(p => p.Id != cp.PreferenceId))
                .ToList();

            var preferenceIdsToAdd = preferenceList
                .Select(p => p.Id)
                .Where(id => customer.CustomerPreferences.All(cp => cp.PreferenceId != id))
                .ToList();

            if (itemsToRemove.Any())
            {
                itemsToRemove.ForEach(cp => customer.CustomerPreferences.Remove(cp));
            }

            if (preferenceIdsToAdd.Any())
            {
                preferenceIdsToAdd.ForEach(preferenceIdToAdd =>
                {
                    customer.CustomerPreferences.Add(new CustomerPreference()
                    {
                        Id = Guid.NewGuid(),
                        CustomerId = customer.Id,
                        PreferenceId = preferenceIdToAdd
                    });
                });
            }

            return customer;
        }
    }
}
