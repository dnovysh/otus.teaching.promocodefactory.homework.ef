using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Otus.Teaching.PromoCodeFactory.DataAccess.Seed;

namespace Otus.Teaching.PromoCodeFactory.WebHost
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var host =CreateHostBuilder(args).Build();
            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;
                var loggerFactory = services.GetRequiredService<ILoggerFactory>();
                var config = services.GetRequiredService<IConfiguration>();
                try
                {
                    IDesignTimeDbContextFactory<StoreContext> dbContextFactory;

                    if (config["SourceDB:Name"] == "SQLite")
                    {
                        dbContextFactory = new Otus.Teaching.PromoCodeFactory.SQLite.EF.DbInit
                            .DesignTimeStoreDbContextFactory();
                    }
                    else
                    {
                        dbContextFactory = new Otus.Teaching.PromoCodeFactory.PostgreSQL.EF.DbInit
                            .DesignTimeStoreDbContextFactory();
                    }

                    await using var storeContext = dbContextFactory
                        .CreateDbContext(new string[] { config.GetConnectionString("DefaultConnection") });
                    await storeContext.Database.MigrateAsync();
                    await StoreContextSeed.SeedAsync(storeContext, loggerFactory);
                }
                catch (Exception ex)
                {
                    var logger = loggerFactory.CreateLogger<Program>();
                    logger.LogError(ex, "An error occured during migration");
                }
            }

            host.Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder => { webBuilder.UseStartup<Startup>(); });
    }
}