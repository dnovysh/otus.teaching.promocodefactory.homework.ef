using Microsoft.Extensions.Configuration;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Helpers
{
    public static class ConnectionStringFactory
    {
        public const string DbConnectionName = @"DefaultConnection";
        public const string SettingFileName = @"appsettings.json";
        public static string GetDbConnectionString()
        {
            IConfiguration config = new ConfigurationBuilder()
                .AddJsonFile(SettingFileName)
                .Build();

            return config.GetConnectionString(DbConnectionName);
        }
    }
}
