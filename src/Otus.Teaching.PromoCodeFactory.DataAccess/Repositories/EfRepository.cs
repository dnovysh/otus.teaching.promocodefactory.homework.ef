using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Specifications;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepository<T> : IRepository<T>
        where T: BaseEntity
    {
        private readonly StoreContext _storeContext;

        public EfRepository(StoreContext storeContext)
        {
            _storeContext = storeContext;
        }
        
        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await _storeContext.Set<T>().ToListAsync();
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            return await _storeContext.Set<T>().FindAsync(id);
        }

        public async Task<IEnumerable<T>> GetByIdAsync(IEnumerable<Guid> ids)
        {
            return await _storeContext.Set<T>().Where(x => ids.Contains(x.Id)).ToListAsync();
        }

        public async Task<T> GetBySpecAsync(ISpecification<T> spec)
        {
            return await ApplySpecification(spec).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<T>> GetAllBySpecAsync(ISpecification<T> spec)
        {
            return await ApplySpecification(spec).ToListAsync();
        }

        public async Task AddAsync(T entity)
        {
            await _storeContext.Set<T>().AddAsync(entity);
            await _storeContext.SaveChangesAsync();
        }

        public async Task AddRangeAsync(IEnumerable<T> entities)
        {
            await  _storeContext.Set<T>().AddRangeAsync(entities);
            await _storeContext.SaveChangesAsync();
        }

        public async Task ReplaceAsync(T entity)
        {
            _storeContext.Set<T>().Update(entity);
            await _storeContext.SaveChangesAsync();
        }

        public async Task RemoveAsync(T entity)
        {
            _storeContext.Set<T>().Remove(entity);
            await _storeContext.SaveChangesAsync();
        }
        
        private IQueryable<T> ApplySpecification(ISpecification<T> spec)
        {
            return SpecificationEvaluator<T>.GetQuery(_storeContext.Set<T>().AsQueryable(), spec);
        }
    }
}
