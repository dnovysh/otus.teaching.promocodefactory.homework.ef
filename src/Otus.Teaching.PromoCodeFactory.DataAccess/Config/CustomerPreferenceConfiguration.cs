using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Config
{
    public class CustomerPreferenceConfiguration: IEntityTypeConfiguration<CustomerPreference>
    {
        public void Configure(EntityTypeBuilder<CustomerPreference> builder)
        {
            builder
                .ToTable("CustomerPreference")
                .HasIndex(cp => new {cp.CustomerId, cp.PreferenceId})
                .HasName("UqCustomerPreference")
                .IsUnique();

            builder
                .Property(cp => cp.Id).ValueGeneratedNever();
            
            builder
                .HasOne(cp => cp.Customer)
                .WithMany(c => c.CustomerPreferences)
                .HasForeignKey(cp => cp.CustomerId)
                .HasConstraintName("FK_CustomerPreference_Customer")
                .OnDelete(DeleteBehavior.ClientCascade);

            builder
                .HasOne(cp => cp.Preference)
                .WithMany()
                .HasForeignKey(cp => cp.PreferenceId)
                .HasConstraintName("FK_CustomerPreference_Preference");
        }
    }
}
