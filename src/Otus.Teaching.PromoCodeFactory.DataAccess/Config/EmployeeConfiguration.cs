using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Config
{
    public class EmployeeConfiguration : IEntityTypeConfiguration<Employee>
    {
        public void Configure(EntityTypeBuilder<Employee> builder)
        {
            builder.Property(e => e.FirstName)
                .HasMaxLength(60)
                .IsRequired();

            builder.Property(e => e.LastName)
                .HasMaxLength(60);

            builder.Property(e => e.Email)
                .HasMaxLength(200);
        }
    }
}