using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Config
{
    public class PromoCodeConfiguration: IEntityTypeConfiguration<PromoCode>
    {
        public void Configure(EntityTypeBuilder<PromoCode> builder)
        {
            builder.Property(pc => pc.Code)
                .HasMaxLength(20)
                .IsRequired();

            builder.Property(pc => pc.ServiceInfo)
                .HasMaxLength(100);
            
            builder.Property(pc => pc.PartnerName)
                .HasMaxLength(100);
            
            builder
                .HasOne(pc => pc.PartnerManager)
                .WithMany()
                .HasForeignKey(pc => pc.PartnerManagerId)
                .HasConstraintName("FK_PromoCode_Employee_PartnerManagerId")
                .OnDelete(DeleteBehavior.ClientSetNull);

            builder
                .HasOne(pc => pc.Preference)
                .WithMany()
                .HasForeignKey(pc => pc.PreferenceId)
                .HasConstraintName("FK_PromoCode_Preference_PreferenceId");

            builder
                .HasOne(pc => pc.Customer)
                .WithMany(c => c.PromoCodes)
                .HasForeignKey(pc => pc.CustomerId)
                .HasConstraintName("FK_PromoCode_Customer_CustomerId")
                .OnDelete(DeleteBehavior.ClientCascade);
        }
    }
}

