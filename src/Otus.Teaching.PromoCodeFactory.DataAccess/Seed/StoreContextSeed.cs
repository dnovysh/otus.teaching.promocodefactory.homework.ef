using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Seed
{
    public class StoreContextSeed
    {
        public static async Task SeedAsync(StoreContext context, ILoggerFactory loggerFactory)
        {
            try
            {
                if (!(await context.Roles.AnyAsync()))
                {
                    await context.AddRangeAsync(FakeDataFactory.Roles);
                    await context.SaveChangesAsync();
                }

                if (!(await context.Employees.AnyAsync()))
                {
                    await context.AddRangeAsync(FakeDataFactory.Employees);
                    await context.SaveChangesAsync();
                }

                if (!(await context.Preferences.AnyAsync()))
                {
                    await context.AddRangeAsync(FakeDataFactory.Preferences);
                    await context.SaveChangesAsync();
                }
                
                if (!(await context.Customers.AnyAsync()))
                {
                    await context.AddRangeAsync(FakeDataFactory.Customers);
                    await context.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                var logger = loggerFactory.CreateLogger<StoreContextSeed>();
                logger.LogError(ex.Message);
            }
        }
    }
}
