﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Specifications;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IRepository<T>
        where T: BaseEntity
    {
        Task<IEnumerable<T>> GetAllAsync();
        
        Task<T> GetByIdAsync(Guid id);
        
        Task<IEnumerable<T>> GetByIdAsync(IEnumerable<Guid> ids);

        Task<T> GetBySpecAsync(ISpecification<T> spec);

        Task<IEnumerable<T>> GetAllBySpecAsync(ISpecification<T> spec);
        
        Task AddAsync(T entity);
        
        Task AddRangeAsync(IEnumerable<T> entities);

        Task ReplaceAsync(T entity);

        Task RemoveAsync(T entity);
    }
}
