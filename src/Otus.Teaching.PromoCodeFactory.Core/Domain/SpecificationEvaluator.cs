using System.Linq;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Specifications;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain
{
    public class SpecificationEvaluator<TEntity> where TEntity : BaseEntity
    {
        public static IQueryable<TEntity> GetQuery(IQueryable<TEntity> inputQuery,
            ISpecification<TEntity> spec)
        {
            var query = inputQuery;
            
            if (spec.Criteria != null)
            {
                query = query.Where(spec.Criteria);
            }

            if (spec.Includes.Any())
            {
                query = spec.Includes.Aggregate(query, 
                    (current, include) => current.Include(include));
            }

            if (spec.IncludePaths.Any())
            {
                query = spec.IncludePaths.Aggregate(query,
                    (current, includePath) => current.Include(includePath));
            }
            
            return query;
        }
    }
}
