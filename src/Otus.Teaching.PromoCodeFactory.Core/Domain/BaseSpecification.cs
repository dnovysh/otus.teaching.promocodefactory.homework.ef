using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Specifications;
    
namespace Otus.Teaching.PromoCodeFactory.Core.Domain
{
    public class BaseSpecification<T> : ISpecification<T>
    {
        public Expression<Func<T, bool>> Criteria { get; private set; }
        
        public List<Expression<Func<T, object>>> Includes { get; } = new List<Expression<Func<T, object>>>();

        public List<string> IncludePaths { get; } = new List<string>();

        public BaseSpecification() { }

        protected BaseSpecification(Expression<Func<T, bool>> criteria)
        {
            Criteria = criteria;
        }

        protected void AddInclude(Expression<Func<T, object>> includeExpression)
        {
            Includes.Add(includeExpression);
        }
        
        protected void AddInclude(string includePath)
        {
            IncludePaths.Add(includePath);
        }
    }
}
