using System;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Specifications
{
    public class CustomerWithCustomerPreferencesSpecification : BaseSpecification<Customer>
    {
        public CustomerWithCustomerPreferencesSpecification(Guid id)
            : base(x => x.Id == id)
        {
            AddInclude(x => x.CustomerPreferences);
        }
    }
}
