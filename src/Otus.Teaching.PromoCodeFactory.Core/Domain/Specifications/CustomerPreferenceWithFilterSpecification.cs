using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Specifications
{
    public class CustomerPreferenceWithFilterSpecification : BaseSpecification<CustomerPreference>
    {
        public CustomerPreferenceWithFilterSpecification(CustomerPreferenceSpecParams customerPreferenceSpecParams)
            : base(customerPreferenceSpecParams.GetFilter())
        {
        }
    }
}
