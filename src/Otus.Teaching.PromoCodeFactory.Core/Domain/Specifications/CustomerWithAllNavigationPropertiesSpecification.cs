using System;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Specifications
{
    public class CustomerWithAllNavigationPropertiesSpecification: BaseSpecification<Customer>
    {
        public CustomerWithAllNavigationPropertiesSpecification(Guid id)
            : base(x => x.Id == id)
        {
            AddInclude(x => x.PromoCodes);
            AddInclude($"{nameof(Customer.CustomerPreferences)}.{nameof(CustomerPreference.Preference)}");
        }
    }
}
