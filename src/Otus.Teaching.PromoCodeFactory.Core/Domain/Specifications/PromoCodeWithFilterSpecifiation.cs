using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Specifications
{
    public class PromoCodeWithFilterSpecification : BaseSpecification<PromoCode>
    {
        public PromoCodeWithFilterSpecification(PromoCodeSpecParams promoCodeParams)
            : base(promoCodeParams.GetFilter())
        {
        }
    }
}
