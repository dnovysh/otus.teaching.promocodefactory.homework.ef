using System;
using System.Linq.Expressions;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Specifications
{
    public class CustomerPreferenceSpecParams
    {
        public Guid? PreferenceId { get; set; }

        public Expression<Func<CustomerPreference, bool>> GetFilter()
        {
            return cp => (!PreferenceId.HasValue || cp.PreferenceId == PreferenceId);
        }
    }
}
