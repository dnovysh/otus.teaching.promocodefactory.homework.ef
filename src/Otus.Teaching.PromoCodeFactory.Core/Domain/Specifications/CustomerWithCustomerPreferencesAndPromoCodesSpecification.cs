using System;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Specifications
{
    public class CustomerWithCustomerPreferencesAndPromoCodesSpecification : BaseSpecification<Customer>
    {
        public CustomerWithCustomerPreferencesAndPromoCodesSpecification(Guid id)
            : base(x => x.Id == id)
        {
            AddInclude(x => x.CustomerPreferences);
            AddInclude(x => x.PromoCodes);
        }
    }
}
