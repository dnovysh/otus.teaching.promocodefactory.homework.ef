using System;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Specifications
{
    public class EmployeeWithRolesSpecification : BaseSpecification<Employee>
    {
        public EmployeeWithRolesSpecification(Guid id)
            : base(x => x.Id == id)
        {
            AddInclude(x => x.Role);
        }
    }
}
