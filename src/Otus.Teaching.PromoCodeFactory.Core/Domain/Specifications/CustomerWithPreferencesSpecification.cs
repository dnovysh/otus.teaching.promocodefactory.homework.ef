using System;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Specifications
{
    public class CustomerWithPreferencesSpecification: BaseSpecification<Customer>
    {
        public CustomerWithPreferencesSpecification(Guid id)
            : base(x => x.Id == id)
        {
            AddInclude($"{nameof(Customer.CustomerPreferences)}.{nameof(CustomerPreference.Preference)}");
        }
    }
}
