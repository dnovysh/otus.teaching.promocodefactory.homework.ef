using System;
using System.Linq.Expressions;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Specifications
{
    public class PromoCodeSpecParams
    {
        public Guid? CustomerId { get; set; }

        public Expression<Func<PromoCode, bool>> GetFilter()
        {
            return pc => (!CustomerId.HasValue || pc.CustomerId == CustomerId);
        }
    }
}
