using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Specifications
{
    public class PreferenceWithFilterSpecification: BaseSpecification<Preference>
    {
        public PreferenceWithFilterSpecification(PreferenceSpecParams preferenceSpecParams)
            : base(preferenceSpecParams.GetFilter())
        {
        }
    }
}
