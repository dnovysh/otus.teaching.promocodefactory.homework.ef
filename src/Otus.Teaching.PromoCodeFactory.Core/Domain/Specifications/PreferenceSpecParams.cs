using System;
using System.Linq.Expressions;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Specifications
{
    public class PreferenceSpecParams
    {
        public string ExactNameMatch { get; set; }

        public Expression<Func<Preference, bool>> GetFilter()
        {
            return p => (string.IsNullOrEmpty(ExactNameMatch) || p.Name == ExactNameMatch);
        }
    }
}
