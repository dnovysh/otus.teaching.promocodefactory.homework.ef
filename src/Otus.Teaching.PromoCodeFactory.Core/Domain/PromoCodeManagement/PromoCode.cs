﻿using System;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class PromoCode
        : BaseEntity
    {
        public string Code { get; set; }

        public string ServiceInfo { get; set; }

        public DateTimeOffset BeginDate { get; set; }

        public DateTimeOffset EndDate { get; set; }

        public string PartnerName { get; set; }

        public Guid? PartnerManagerId { get; set; }
        public Employee PartnerManager { get; set; }

        public Guid PreferenceId { get; set; }
        public Preference Preference { get; set; }
        
        public Guid CustomerId { get; set; }
        public Customer Customer { get; set; }

        public PromoCode(Guid id,
            string code,
            string serviceInfo,
            DateTimeOffset beginDate,
            DateTimeOffset endDate,
            string partnerName,
            Guid preferenceId,
            Guid customerId)
        {
            Id = id;
            Code = code;
            ServiceInfo = serviceInfo;
            BeginDate = beginDate;
            EndDate = endDate;
            PartnerName = partnerName;
            PreferenceId = preferenceId;
            CustomerId = customerId;
        }
    }
}
