using System;
using System.Reflection;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.DataAccess.Helpers;

namespace Otus.Teaching.PromoCodeFactory.PostgreSQL.EF.DbInit
{
    public class DbInitContextFactory<T> where T : DbContext
    {
        public static T CreateDbContext(string appConnectionString)
        {
            var connectionString = appConnectionString ?? ConnectionStringFactory.GetDbConnectionString();
            var migrationAssembly = typeof(Program).GetTypeInfo().Assembly.GetName().Name;
            var builder = new DbContextOptionsBuilder<T>();
            builder.UseNpgsql(
                connectionString,
                ob => ob.MigrationsAssembly(migrationAssembly));
            var constructor = typeof(T).GetConstructor(new Type[] { typeof(DbContextOptions<T>) });
            return (T)constructor?.Invoke(new object[] { builder.Options });
        }
    }
}
