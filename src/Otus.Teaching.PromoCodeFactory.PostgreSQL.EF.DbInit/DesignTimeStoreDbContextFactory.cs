using System;
using Microsoft.EntityFrameworkCore.Design;
using Otus.Teaching.PromoCodeFactory.DataAccess;

namespace Otus.Teaching.PromoCodeFactory.PostgreSQL.EF.DbInit
{
    public class DesignTimeStoreDbContextFactory: IDesignTimeDbContextFactory<StoreContext>
    {
        public StoreContext CreateDbContext(string[] args)
        {
            string appConnectionString;
                
            if (args.Length > 0 && !string.IsNullOrEmpty(args[0]))
            {
                appConnectionString = args[0];
            }
            else
            {
                appConnectionString = null;
            }
            
            return DbInitContextFactory<StoreContext>.CreateDbContext(appConnectionString);
        }
    }
}
